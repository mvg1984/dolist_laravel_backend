<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Models
use App\Models\Todo\Todo;

// Job
use App\Jobs\TodoReminder;

class RunReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends reminders about todo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $todos = Todo::reminders();
        foreach ($todos as $todo)
        {    
            TodoReminder::dispatch($todo);
        }
    }
}
