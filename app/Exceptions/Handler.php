<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')) 
        {       
            if($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException || $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
            {
                $code = 404;
            }
            elseif($exception instanceof \BadMethodCallException || $exception instanceof \ErrorException)
            {
                $code = 500;
            }
            else
            {
                $code = 400;
            }

            if(env('APP_DEBUG', true))
            {
                return response()->json([
                    'result' => false,
                    'data' => $exception->getMessage() == '' ? 'Not found' : $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'exception' => get_class($exception),
                ], $code);
            }
            else
            {
                return response()->json([
                    'result' => false,
                    'data' => $exception->getMessage(),
                ], $code);                
            }
        }

        return parent::render($request, $exception);
    }
}
