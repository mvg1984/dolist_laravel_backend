<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Mail;
 
class Api 
{
    /**
     * Standartize response from API
     * 
     * @param bool $result
     * @param mixed $data
     * 
     * @return array
     */
    public static function response($result = true, $data = null) 
    {
        return [
            'result' => $result,
            'data' => $data
        ];
    }

    /**
     * Send emails 
     * 
     * @param bool $result
     * @param mixed $data
     * 
     * @return bool
     */    
    public static function email($subject, $to, $template, $bounds = [])
    {
        try 
        {
            Mail::send($template, $bounds, function ($message) use ($subject, $to) {
                $message->subject($subject)->to($to, $to);
            });

            return true;
        } 
        catch (\Exception $e) 
        {
            return false;
        }
    }

    /**
     * Get current token
     * 
     * @return string
     */    
    public static function token()
    {
        return defined('token') ? token : null;
    }

    /**
     * Get current user id
     * 
     * @return int
     */     
    public static function userId()
    {
        return defined('user_id') ? user_id : null;
    }   
    
    /**
     * Get upload path for todo
     * 
     * @param int $todoId
     * 
     * @return string
     */      
    public static function todoFolder(int $todoId)
    {
        return 'public/'.self::userId().'/'.$todoId;
    }
}