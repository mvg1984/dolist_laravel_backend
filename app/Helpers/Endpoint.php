<?php

namespace App\Helpers;

use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Session\Session as SessionStorage;
use Symfony\Component\HttpFoundation\StreamedResponse;

// Guzzle
use GuzzleHttp\Client;

// Helpers
use Session;

class Endpoint 
{
    /**
     * Registration
     * 
     * @param array $payload
     * 
     * @return array
     */
    public static function registration(array $payload) 
    {
        $payload['activationLink'] = route('users.activation.action', ['hash' => '']).'/';

        $response = self::post(env('APP_API_URL').'/users/registration', $payload);

        return $response;
    }  

    /**
     * Authorization
     * 
     * @param array $payload
     * 
     * @return array
     */
    public static function authorization(array $payload) 
    {
        $response = self::post(env('APP_API_URL').'/users/authorization', $payload);
        if($response['result'])
        {
            Session::set('token', $response['data']);
        }

        return $response;
    }   

    /**
     * Logout
     * 
     * 
     * @return array
     */
    public static function logout() 
    {
        $response = self::delete(env('APP_API_URL').'/profile/logout');

        if($response['result'])
        {
            Session::set('token', null);
        }

        return $response;
    }  

    /**
     * Activation
     * 
     * @param string $hash
     * 
     * @return array
     */
    public static function activation($hash) 
    {
        return self::get(env('APP_API_URL').'/users/activation/'.$hash);
    }  

    /**
     * Profile information
     * 
     * @return array
     */
    public static function profile() 
    {
        return self::get(env('APP_API_URL').'/profile');
    }  

    /**
     * Todo list
     * 
     * @param array $parameters
     * 
     * @return array
     */
    public static function todoList(array $parameters = []) 
    {
        return self::get(env('APP_API_URL').'/todos', $parameters);
    }  

    /**
     * Todo remove
     * 
     * @param int $id
     * 
     * @return array
     */
    public static function todoRemove(int $id) 
    {
        return self::delete(env('APP_API_URL').'/todos/'.$id);
    }   

    /**
     * Todo create
     * 
     * @param array $payload
     * 
     * @return array
     */
    public static function todoCreate(array $payload) 
    {
        return self::post(env('APP_API_URL').'/todos', $payload);
    } 

    /**
     * Todo update
     * 
     * @param int $id
     * @param array $payload
     * 
     * @return array
     */
    public static function todoUpdate(int $id, array $payload) 
    {
        return self::put(env('APP_API_URL').'/todos/'.$id, $payload);
    }     
    
    /**
     * Get todo
     * 
     * @param int $id
     * 
     * @return array
     */
    public static function todoItem(int $id) 
    {
        return self::get(env('APP_API_URL').'/todos/'.$id);
    } 

    /**
     * Upload file to todo
     * 
     * @param int $todoId
     * @param string $name
     * @param array $content
     * 
     * @return array
     */
    public static function todoUpload(int $todoId, string $name, $content) 
    {
        $response = self::postMultipart(env('APP_API_URL').'/todos/'.$todoId.'/file', [
            'headers' => [
                'Accept' => 'application/json',
                'Dolist-Token' => Session::get('token')
            ],
            'multipart' => [[
                'name' => 'file',
                'contents' => $content,
                'filename' => $name
            ]],
            'http_errors' => false,
        ]);

        return $response;
    }   
    
    /**
     * Remove file
     * 
     * @param int $todoId
     * @param int $fileId
     * 
     * @return array
     */
    public static function todoFilesRemove(int $todoId, int $fileId) 
    {
        return self::delete(env('APP_API_URL').'/todos/'.$todoId.'/file/'.$fileId);
    }  

    /**
     * Get todo files
     * 
     * @param int $id
     * 
     * @return array
     */
    public static function todoFiles(int $id) 
    {
        return self::get(env('APP_API_URL').'/todos/'.$id.'/file');
    }  
    
    /**
     * Get todo file
     * 
     * @param int $id
     * @param int $fileId
     * 
     * @return array
     */
    public static function download(int $id, int $fileId) 
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->get(env('APP_API_URL').'/todos/'.$id.'/file/'.$fileId, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Dolist-Token' => Session::get('token')
            ],
            'http_errors' => false,
        ]);   

        // Detect file name
        $files = self::todoFiles($id);
        if ($files['result']) 
        {
            $file = collect($files['data'])->filter(function($item) use ($fileId) {
                return $item['id'] == $fileId;
            })->first();        
        };

        if(!isset($file) || !$file) 
        {
            abort(404);
        }
        
        return [
           'binary' => $response->getBody()->getContents(),
           'name' => $file['label']
        ];
    }     
    
    /**
     * Get dictionary for rodo
     * 
     * @param string $type
     * 
     * @return array
     */
    public static function dictionary(string $type) 
    {
        return self::get(env('APP_API_URL').'/todos/dictionaries/'.$type);
    }   
    
    /**
     * Get authirzed devices
     * 
     * @return array
     */
    public static function devices() 
    {
        return self::get(env('APP_API_URL').'/profile/devices/');
    }   
    
    /**
     * Delete device
     * 
     * @param int $id
     * 
     * @return array
     */
    public static function deleteDevice(int $id) 
    {
        return self::delete(env('APP_API_URL').'/profile/devices/'.$id);
    }      
    
    /**
     * Change profile name
     * 
     * @param array $payload
     * 
     * @return array
     */
    public static function changeName(array $payload) 
    {
        return self::put(env('APP_API_URL').'/profile/change/name', $payload);
    }  
    
    /**
     * Change profile password
     * 
     * @param array $payload
     * 
     * @return array
     */
    public static function changePassword(array $payload) 
    {
        return self::put(env('APP_API_URL').'/profile/change/password', $payload);
    }     

    /* Requests methods */

    /**
     * POST
     * 
     * @param string $url
     * @param array $payload
     * 
     * @return array
     */
    public static function post(string $url, array $payload) 
    {
        $options = self::options();
        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, [
                \GuzzleHttp\RequestOptions::JSON => $payload,
                'headers' => $options['headers'],
                'http_errors' => $options['http_errors']
            ]
        );
    
        return self::normalize($response->getBody());
    }

    /**
     * POST Multipart
     * 
     * @param string $url
     * @param array $payload
     * 
     * @return array
     */
    public static function postMultipart(string $url, array $payload) 
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, $payload);
    
        return self::normalize($response->getBody());
    }

    /**
     * POST
     * 
     * @param string $url
     * @param array $payload
     * 
     * @return array
     */
    public static function put(string $url, array $payload) 
    {
        $options =  self::options();
        $options['json'] = $payload;

        $client = new \GuzzleHttp\Client();
        $response = $client->put($url, $options);

        return self::normalize($response->getBody());
    }    

    /**
     * DELETE
     * 
     * @param string $url
     * 
     * @return array
     */
    public static function delete(string $url) 
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->delete($url, self::options());
    
        return self::normalize($response->getBody());
    }

    /**
     * GET
     * 
     * @param string $url
     * @param array $parameters
     * 
     * @return array
     */
    public static function get(string $url, array $parameters = []) 
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url.'?'.http_build_query($parameters), self::options());
    
        return self::normalize($response->getBody());
    }

    /**
     * Normalize response from API
     * 
     * @param string $data
     * 
     * @return array
     */
    public static function normalize(string $data)
    {
        $response = json_decode($data, true);

        if(!$response)
        {
            $response = [
                'result' => false,
                'data' => 'API is unavailable'
            ];
        }

        if(isset($response['message']))
        {
            $response['data'] = $response['message'];
        }

        return $response;
    }

    /**
     * Client options
     * 
     * @return array
     */    
    public static function options()
    {
        return [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Dolist-Token' => Session::get('token')
            ],
            'http_errors' => false,
        ];
    }
}