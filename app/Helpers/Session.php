<?php

namespace App\Helpers;

use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Session\Session as SessionStorage;

class Session 
{
    /**
     * Set value to session
     * 
     * @param string $key
     * @param any $value
     * 
     * @return void
     */
    public static function set(string $key, $value) 
    {
        $session = new SessionStorage();
        $session->set($key, $value);
    }

    /**
     * Get value from session
     * 
     * @param string $key
     * 
     * @return any || null
     */
    public static function get(string $key) 
    {
        $session = new SessionStorage();
        return $session->get($key);
    }
}