<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\User\User;
use App\Models\User\Token;

// Facades
use Validator;

// Helpers
use Api;

class ProfileController extends Controller
{
    private $userId;
    private $token;
    
    function __construct() 
    {
        $this->middleware(function ($request, $next) {
            $this->userId = Api::userId();        
            $this->token = Api::token();        
            return $next($request);
        });        
    }

    /**
     * Profile information
     * 
     * @param Request $request
     * 
     * @return string
     */    
    public function index(Request $request)
    {
        return Api::response(true, User::find($this->userId));          
    }

    /**
     * Logout
     * 
     * @param Request $request
     * 
     * @return string
     */    
    public function logout(Request $request)
    {
        return Api::response(true, Token::deleteByToken($this->token));          
    }

    /**
     * Logout from all devices
     * 
     * @param Request $request
     * 
     * @return string
     */    
    public function logoutAll(Request $request)
    {
        return Api::response(true, Token::deleteByUser($this->userId));          
    }    

    /**
     * Logout
     * 
     * @param Request $request
     * 
     * @return string
     */    
    public function devices(Request $request)
    {
        return Api::response(true, Token::devices($this->userId));           
    }    

    /**
     * Logout from all devices
     * 
     * @param Request $request
     * @param int $deviceId
     * 
     * @return string
     */    
    public function logoutDevice(Request $request, int $deviceId)
    {
        return Api::response(true, Token::deleteByDevice($this->userId, $deviceId));          
    }  
    
    /**
     * Change user's name
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function changeName(Request $request)
    {
        // Body
        $input = $request->json()->all();

        // Validation rules
        $validator = Validator::make($input, [
            'name' => 'required|max:191',
        ]);

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $user = User::find($this->userId)->update($input);
            $result = true;
            $response = true;
        }

        // Response
        return Api::response($result, $response);    
    } 
    
    /**
     * Change user's password
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function changePassword(Request $request)
    {
        // Body
        $input = $request->json()->all();

        // Validation rules
        $validator = Validator::make($input, [
            'password' => 'required|min:5',
        ]);

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $user = User::find($this->userId)->update($input);
            $result = true;
            $response = true;
        }

        // Response
        return Api::response($result, $response);    
    }     
}
