<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\Todo\Todo;
use App\Models\Todo\Color;
use App\Models\Todo\Group;
use App\Models\Todo\Importance;

// Facades
use Validator;
use Response;
use Storage;

// Helpers
use Api;

class TodoController extends Controller
{
    private $userId;
    
    function __construct() 
    {
        $this->middleware(function ($request, $next) {
            $this->userId = Api::userId();        
            return $next($request);
        });        
    }

    /**
     * List of the todos
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function index(Request $request)
    {
        // Response
        return Api::response(true, Todo::todos($this->userId));    
    }

    /**
     * New todo creation
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function create(Request $request)
    {
        // Body
        $input = $request->json()->all();      

        // Validation rules
        $validator = Validator::make($input, [
            'group_id' => 'required|integer|exists:groups,id',
            'importance_id' => 'required|integer|exists:importances,id',
            'colors_id' => 'present|array',
            'name' => 'required|max:191',
            'deadline_at' => 'nullable|date',
            'reminder_at' => 'nullable|date',
        ]);

        // Validate colors for pivot
        if (!$validator->fails())
        {
            foreach ($input['colors_id'] as $colorId)
            {
                $validator = Validator::make(['color_id' => $colorId], [
                    'color_id' => 'required|integer|exists:colors,id',
                ]);

                if ($validator->fails())
                {
                    break;
                }
            }
        }

        // Extend input required data
        $input['user_id'] = $this->userId;          

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $todo = Todo::store($input);
            $result = true;
            $response = $todo->id;
        }

        // Response
        return Api::response($result, $response);    
    }

    /**
     * Single todo
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return string
     */
    public function show(Request $request, int $id)
    {
        // Response
        return Api::response(true, Todo::todo($this->userId, $id));    
    }    

    /**
     * New todo creation
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return string
     */
    public function update(Request $request, int $id)
    {       
        // Body
        $input = $request->json()->all();      

        // Validation rules
        $validator = Validator::make($input, [
            'group_id' => 'required|integer|exists:groups,id',
            'importance_id' => 'required|integer|exists:importances,id',
            'colors_id' => 'present|array',
            'name' => 'required|max:191',
            'deadline_at' => 'nullable|date',
            'reminder_at' => 'nullable|date',
        ]);

        // Validate colors for pivot
        if (!$validator->fails())
        {
            foreach ($input['colors_id'] as $colorId)
            {
                $validator = Validator::make(['color_id' => $colorId], [
                    'color_id' => 'required|integer|exists:colors,id',
                ]);

                if ($validator->fails())
                {
                    break;
                }
            }
        }        

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $todo = Todo::modify($this->userId, $id, $input);
            $result = true;
            $response = $todo->id;
        }

        // Response
        return Api::response($result, $response);    
    }    
    
    /**
     * Remove todo
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return string
     */
    public function destroy(Request $request, int $id)
    {
        Todo::remove($this->userId, $id);

        // Response
        return Api::response(true, true);    
    }       

    /* Dictionaries */

    /**
     * List of the colors
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function colors(Request $request)
    {
        // Response
        return Api::response(true, Color::get());    
    }    
    
    /**
     * List of the groups
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function groups(Request $request)
    {
        // Response
        return Api::response(true, Group::get());    
    }    
    
    /**
     * List of the importances
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function importances(Request $request)
    {
        // Response
        return Api::response(true, Importance::get());    
    }     

    /* Files */

    /**
     * Attach file to the todo
     * 
     * @param Request $request
     * @param int $id TodoId
     * 
     * @return string
     */
    public function attach(Request $request, int $id)
    {
        $todo = Todo::todo($this->userId, $id);

        // Default answer
        $result = false;
        $response = false;

        // If we have the file
        if ($request->hasFile('file'))
        {
            $file = $request->file('file');

            // Validation rules
            $validator = Validator::make($request->all(), [
                'file' => 'mimes:doc,pdf,docx,zip,png,mp4,xls,txt,jpg,jpeg'
            ]);

            if ($validator->fails())
            {
                $result = false;
                $response = $validator->errors()->first();
            }  
            else
            {
                $model = [
                    'todo_id' => $id,
                    'label' => $file->getClientOriginalName(),
                    'file_name' => md5(microtime()),
                    'size' => $file->getSize(),
                    'type' => mb_strtolower($file->getClientOriginalExtension()),
                ];

                // Store in the folder
                $file->storeAs(Api::todoFolder($id), $model['file_name']);

                // Store in the database
                $todo->addFile($model);

                $result = true;
                $response = true;
            }
        }

        // Response
        return Api::response($result, $response);    
    }  
    
    /**
     * Download file from the todo
     * 
     * @param Request $request
     * @param int $id TodoId
     * @param int $fileId FileId
     * 
     * @return stream
     */
    public function download(Request $request, int $id, int $fileId)
    {       
        $file = Todo::todo($this->userId, $id)->getFile($fileId);

        // Response
        return Response::download(
            $file->path, 
            $file->caption
        );  
    }  
    
    /**
     * File list
     * 
     * @param Request $request
     * @param int $id TodoId
     * 
     * @return array
     */
    public function files(Request $request, int $id)
    {        
        $files = Todo::todo($this->userId, $id)->files;

        // Response
        return Api::response(true, $files); 
    }   
    
   /**
     * Remove files
     * 
     * @param Request $request
     * @param int $id TodoId
     * @param int $fileId FileId
     * 
     * @return stream
     */
    public function erase(Request $request, int $id, int $fileId)
    {        
        Todo::todo($this->userId, $id)->getFile($fileId)->remove();

        // Response
        return Api::response(true, true);  
    }      

    /**
     * List of the todos for notifications
     * 
     * @return string
     */
    public function notifications(Request $request)
    {
        // Response
        return Api::response(true, Todo::reminders());    
    }     
}
