<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\User\User;

// Facades
use Validator;

// Helpers
use Api;

class UserController extends Controller
{
    /**
     * New user creation
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function registration(Request $request)
    {
        // Body
        $input = $request->json()->all();

        // Validation rules
        $validator = Validator::make($input, [
            'name' => 'required|max:191',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5',
            'activationLink' => 'required',
        ]);

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $user = User::registration($input);
            $result = true;
            $response = $user->id;

            // Send the activation link to the user
            Api::email('Welcome to Dolist', $user->email, 'emails.registration', [
                'activationLink' => $input['activationLink'].md5($user->email), // Dirty, i know, just for test case
                'userName' => $user->name,
            ]);
        }

        // Response
        return Api::response($result, $response);    
    }

    /**
     * New user activation
     * 
     * @param string $hash
     * 
     * @return string
     */
    public function activation(string $hash)
    {    
        $activation = User::activation($hash);

        if (!$activation)
        {
            $result = false;
            $response = 'Activation hash not been found or account has been activated already';
        }
        else 
        {
            $result = true;
            $response = $activation;
        }

        // Response
        return Api::response($result, $response);          
    }

    /**
     * New user creation
     * 
     * @param Request $request
     * 
     * @return string
     */
    public function authorization(Request $request)
    {
        // Body
        $input = $request->json()->all();

        // Validation rules
        $validator = Validator::make($input, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:5',
        ]);

        if ($validator->fails())
        {
            $result = false;
            $response = $validator->errors()->first();
        }        
        else
        {
            $token = User::token($input);

            if (!$token)
            {
                $result = false;
                $response = 'User not found or has not been activated yet';
            }
            else 
            {
                $result = true;
                $response = $token;
            }
        }

        // Response
        return Api::response($result, $response);    
    }  
}
