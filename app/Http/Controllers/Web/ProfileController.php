<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Helpers
use Endpoint;

class ProfileController extends Controller
{
    /**
     * Log in view
     * 
     * @param Request $request
     * 
     * @return View
     */  
    public function index(Request $request)
    {
        $response = Endpoint::devices();

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }  
        
        $devices = $response['data'];

        return view('profile.index',[
            'devices' => $devices
        ]);
    }

    /**
     * Change name
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function changeName(Request $request)
    {
        $response = Endpoint::changeName($request->all()); 

        if ($response['result'])
        {
            return redirect()->back()->with('success', 'Your name has been changed');
        }
        else 
        {
            return redirect()->back()->with('error', $response['data'])->withInput();
        }
    }  
    
    /**
     * Change password
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function changePassword(Request $request)
    {
        $response = Endpoint::changePassword($request->all());

        if ($response['result'])
        {
            return redirect()->back()->with('success', 'Your password has been changed');
        }
        else 
        {
            return redirect()->back()->with('error', $response['data'])->withInput();
        }
    } 
    
    /**
     * Delete device
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Redirect
     */  
    public function deleteDevice(Request $request, int $id)
    {
        $response = Endpoint::deleteDevice($id);

        if ($response['result'])
        {
            return redirect()->back()->with('success', 'Device has been logged out');
        }
        else 
        {
            return redirect()->back()->with('error', $response['data'])->withInput();
        }
    }     
}
