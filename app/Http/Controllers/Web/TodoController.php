<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Helpers
use Endpoint;

class TodoController extends Controller
{
    /**
     * List of the todo
     * 
     * @param Request $request
     * 
     * @return View
     */  
    public function index(Request $request)
    {
        $response = Endpoint::todoList($request->all());

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        return view('todos.index', [
            'list' => $response['data']
        ]);
    }

    /**
     * View for the new todo
     * 
     * @param Request $request
     * 
     * @return View
     */  
    public function new(Request $request)
    {
        // Dictionaries
        $dictionaries = [
            'importances' => [],
            'colors' => [],
            'groups' => [],
        ];

        foreach ($dictionaries as $type => $dictionary)
        {
            $response = Endpoint::dictionary($type);

            if (!$response['result'])
            {
                return view('layouts.error', ['error' => $response['data']]);
            }  
            
            $dictionaries[$type] = $response['data'];
        }

        // Response
        return view('todos.item', [
            'importances' => $dictionaries['importances'],
            'colors' => $dictionaries['colors'],
            'groups' => $dictionaries['groups'],
        ]);
    }  
    
    /**
     * Update todo
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function create(Request $request)
    {
        $model = $this->_modifyDates($request->all());

        $response = Endpoint::todoCreate($model);

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        // New todo id
        $id = $response['data'];

        // Upload the file if we have one
        if(($result = $this->_uploadFile($request, $id)) !== true)
        {
            return view('layouts.error', ['error' => $result]);
        }

        return redirect()->route('todos.show', ['id' => $id]);
    }  

    /**
     * Show the todo
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return View
     */  
    public function show(Request $request, int $id)
    {
        // Todo
        $response = Endpoint::todoItem($id);

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        $item = $response['data'];

        // Prepare colors
        $selectedIds = [];
        
        foreach ($item['colors'] as $color)
        {
            $selectedIds[] = $color['id'];
        }

        $item['colors'] = $selectedIds;

        // Dictionaries
        $dictionaries = [
            'importances' => [],
            'colors' => [],
            'groups' => [],
        ];

        foreach ($dictionaries as $type => $dictionary)
        {
            $response = Endpoint::dictionary($type);

            if (!$response['result'])
            {
                return view('layouts.error', ['error' => $response['data']]);
            }  
            
            $dictionaries[$type] = $response['data'];
        }

        // Files
        $response = Endpoint::todoFiles($id);

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        $files = $response['data'];

        // Response
        return view('todos.item', [
            'item' => $item,
            'files' => $files,
            'importances' => $dictionaries['importances'],
            'colors' => $dictionaries['colors'],
            'groups' => $dictionaries['groups'],
        ]);
    }    

    /**
     * Remove the todo
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Redirect
     */  
    public function remove(Request $request, int $id)
    {
        $response = Endpoint::todoRemove($id);
        
        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        return redirect()->back();
    }    

    /**
     * Download the file
     * 
     * @param Request $request
     * @param int $id
     * @param int $fileId
     * 
     * @return View
     */  
    public function downloadFile(Request $request, int $id, int $fileId)
    {
        $response = Endpoint::download($id, $fileId);

        return response($response['binary'], 200, [
            'Content-Disposition' => 'attachment; filename="'.$response['name'].'"',
        ]);        
    }   
    
    /**
     * Update todo
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Redirect
     */  
    public function update(Request $request, int $id)
    {
        $model = $this->_modifyDates($request->all());

        $response = Endpoint::todoUpdate($id, $model);

        if (!$response['result'])
        {
            return view('layouts.error', ['error' => $response['data']]);
        }

        // Upload the file if we have one
        if(($result = $this->_uploadFile($request, $id)) !== true)
        {
            return view('layouts.error', ['error' => $result]);
        }

        return redirect()->back();
    }  
    
    /**
     * Remove the file
     * 
     * @param Request $request
     * @param int $id
     * @param int $fileId
     * 
     * @return View
     */  
    public function removeFile(Request $request, int $id, int $fileId)
    {
        Endpoint::todoFilesRemove($id, $fileId);

        return redirect()->back();        
    }   
    
    /**
     * Modify dates in model
     * 
     * @param array $fileId
     * 
     * @return array
     */  
    private function _modifyDates(array $model)
    {
        $dates = ['deadline', 'reminder'];
        foreach ($dates as $date)
        {
            if (isset($model[$date.'_date']))
            {
                $model[$date.'_at'] = date('Y-m-d', strtotime($model[$date.'_date'])).' '.$model[$date.'_time'].':00';
            }
            else
            {
                $model[$date.'_at'] = null; 
            }
        }

        return $model;
    }

    /**
     * Modify dates in model
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return void
     */ 
    private function _uploadFile(Request $request, int $id)
    {
        $file = $request->file('file');
        if ($file) 
        {
            $response = Endpoint::todoUpload($id, $file->getClientOriginalName(), file_get_contents($file->getRealPath()));

            if (!$response['result'])
            {
                return $response['data'];
            }
        }

        return true;
    }
}
