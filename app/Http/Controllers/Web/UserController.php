<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Helpers
use Endpoint;

class UserController extends Controller
{
    /**
     * Log in view
     * 
     * @param Request $request
     * 
     * @return View
     */  
    public function authorization(Request $request)
    {
        return view('users.authorization');
    }

    /**
     * Log in action
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function _authorization(Request $request)
    {
        $response = Endpoint::authorization($request->all()); // Authorization in API

        if ($response['result'])
        {
            return redirect()->route('todos.index');
        }
        else 
        {
            return redirect()->back()->with('error', $response['data'])->withInput();
        }
    }

    /**
     * Log out action
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function _logout(Request $request)
    {
        Endpoint::logout(); // Logout from API

        return redirect()->route('todos.index');
    }

    /**
     * Registration view
     * 
     * @param Request $request
     * 
     * @return View
     */  
    public function registration(Request $request)
    {
        return view('users.registration');
    }

    /**
     * Registration action
     * 
     * @param Request $request
     * 
     * @return Redirect
     */  
    public function _registration(Request $request)
    {
        $response = Endpoint::registration($request->all()); // Authorization in API

        if ($response['result'])
        {
            return redirect()->route('users.authorization.view')->with('success', 'Check your email for the activation link');
        }
        else 
        {
            return redirect()->back()->with('error', $response['data'])->withInput();
        }
    }

    /**
     * Activation
     * 
     * @param Request $request
     * @param string $hash
     * 
     * @return string
     */    
    public function activation(Request $request, string $hash)
    {
        $response = Endpoint::activation($hash); 

        if ($response['result'])
        {
            return redirect()->route('users.authorization.view')->with('success', 'Account activated');
        }
        else 
        {
            return redirect()->route('users.authorization.view')->with('error', 'Activation link is wrong or it has been used already');
        }       
    }
}
