<?php

namespace App\Http\Middleware;

// Models
use App\Models\User\Token;

// Facades
use Closure;
use Response; 

// Helpers
use Session;

class Anonymous
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('token')) 
        {
            return redirect()->route('todos.index');
        }

        return $next($request);
    }
}
