<?php

namespace App\Http\Middleware;

// Models
use App\Models\User\Token;

// Facades
use Closure;
use Response; 
use View;

// Helpers
use Endpoint;
use Session;

class Authorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('token')) 
        {
            return redirect()->route('users.authorization.view');
        }

        // Share user's name among all views
        $profile = Endpoint::profile();
        
        if(!$profile['result'])
        {
            Session::set('token', null);
            return redirect()->route('todos.index');
        }
        
        View::share('userName', $profile['data']['name']);
        View::share('channel', $profile['data']['channel']);

        return $next($request);
    }
}
