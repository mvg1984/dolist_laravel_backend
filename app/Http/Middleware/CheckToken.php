<?php

namespace App\Http\Middleware;

// Models
use App\Models\User\Token;

// Facades
use Closure;
use Response; 

// Helpers
use Api;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!request()->headers->has('Dolist-Token'))
        {  
            return Response::json(Api::response(false, 'Dolist token missed in the headers'));  
        }  
        
        if (!($token = Token::where('token', request()->header('Dolist-Token'))->first()))
        {
            return Response::json(Api::response(false, 'Dolist token is incorrect')); 
        }

        // Update last usage
        $token->touch();

        // Make user id and token as global constants
        define('user_id', $token->user_id);
        define('token', request()->header('Dolist-Token'));

        return $next($request);
    }
}
