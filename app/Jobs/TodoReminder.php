<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// Helpers
use Api;

class TodoReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    public $todo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($todo)
    {
        $this->todo = $todo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->consoleLog($this->todo->name);
        $this->consoleLog($this->todo->user->email);

        // Send notification message
        Api::email('Dolist', $this->todo->user->email, 'emails.notification', [
            'todo' => $this->todo,
        ]);

        // Push
        event(new \App\Events\ReminderSent($this->todo->user->channel, $this->todo));

        $this->consoleLog('DONE');
        $this->consoleLog('-----');
    }

    /**
     * Out to console.
     *
     * @return void
     */
    private function consoleLog(string $info)
    {
        echo $info;
        echo PHP_EOL;
    }    
}
