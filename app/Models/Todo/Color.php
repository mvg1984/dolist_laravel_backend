<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    // Related table
    protected $table = 'colors';

    // Timestamp columns usage
    public $timestamps = false;

    // Fillable fields
    protected $fillable = [
        'name',
        'code',
    ];

    // Hidden columns
    protected $hidden = ['pivot'];

    /* Relations */

    /*
     * The todos that belong to the color.
     */
    public function todos()
    {
        return $this->belongsToMany('App\Models\Todo\Todo', 'colors_todos', 'color_id', 'todo_id');
    } 
}
