<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

// Facades
use Storage;

// Helpers
use Api;

class File extends Model
{
    // Related table
    protected $table = 'todos_files';

    // Timestamp columns usage
    public $timestamps = true;

    // Fillable fields
    protected $fillable = [
        'todo_id',
        'label',
        'file_name',
        'size',
        'type',
    ];

    // Hidden columns
    protected $hidden = [
        'todo_id', 
        'updated_at',
        'file_name',
    ];       

    /* Accessors */

    /**
     * Get local path
     *
     * @return string
     */
    public function getPathAttribute()
    {
        $path = Api::todoFolder($this->todo_id).'/'.$this->file_name;
        return Storage::disk('local')->path($path);
    }

    /* Methods */

    /**
     * Get file by id
     * 
     * @param int $id
     * 
     * @return File
     */    
    public function getById(int $id)  
    {
        return $this->findOrFail($id);
    }  

    /**
     * Remove file
     * 
     * @return void
     */    
    public function remove()  
    {
        unlink($this->path);    
        $this->delete();
    }     
}
