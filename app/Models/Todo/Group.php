<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    // Related table
    protected $table = 'groups';

    // Timestamp columns usage
    public $timestamps = false;

    // Fillable fields
    protected $fillable = [
        'name',
        'code',
    ];
}
