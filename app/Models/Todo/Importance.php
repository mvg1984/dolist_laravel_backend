<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

class Importance extends Model
{
    // Related table
    protected $table = 'importances';

    // Timestamp columns usage
    public $timestamps = false;

    // Fillable fields
    protected $fillable = [
        'name',
        'code',
    ];
}
