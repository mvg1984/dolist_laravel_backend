<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Todo extends Model
{
    // Related table
    protected $table = 'todos';

    // Timestamp columns usage
    public $timestamps = true;

    // Fillable fields
    protected $fillable = [
        'user_id',
        'group_id',
        'importance_id',
        'name',
        'description',
        'deadline_at',
        'reminder_at',
        'is_reminded',
    ];

    // Hidden columns
    protected $hidden = [
        'pivot', 
        'user_id',
        // 'importance_id',
        // 'group_id',
        'is_reminded',
    ];    

    /* Methods */

    /**
     * Create todo
     * 
     * @param array $input
     * 
     * @return Todo
     */    
    public static function store(array $input) 
    {
        $todo = self::create($input);
        $todo->colors()->attach($input['colors_id']);

        return $todo;
    }   

    /**
     * Update todo
     * 
     * @param int $userId
     * @param int $todoId
     * @param array $input
     * 
     * @return Todo
     */    
    public static function modify(int $userId, int $todoId, array $input) 
    {
        $input['is_reminded'] = false; // Reset reminder

        $todo = self::todo($userId, $todoId);
        $todo->update($input);
        $todo->colors()->sync($input['colors_id']);

        return $todo;
    }     

    /**
     * Remove todo
     * 
     * @param int $userId
     * @param int $todoId
     * @param array $input
     * 
     * @return bool
     */    
    public static function remove(int $userId, int $todoId) 
    {
        $todo = self::todo($userId, $todoId);
        $todo->colors()->detach();
        $todo->delete();

        return true;
    }      
    
    /**
     * List of the todos by user
     * 
     * @param int $userId
     * 
     * @return array[Todo]
     */    
    public static function todos(int $userId) 
    {
        return self::list($userId)->get();
    }   
    
    /**
     * Single todo
     * 
     * @param int $userId
     * 
     * @return Todo
     */    
    public static function todo(int $userId, int $todoId) 
    {
        $todo = self::list($userId)->where('id', $todoId)->first();
        if(!$todo) 
        {
            abort(404, 'Todo not found');
        }

        return $todo;
    }  

    /**
     * Add file to todo
     * 
     * @param array $model
     * 
     * @return void
     */    
    public function addFile(array $model) 
    {
        $this->files()->create($model);
    }   
    
    /**
     * Add file to todo
     * 
     * @param int $id file id
     * 
     * @return string
     */    
    public function getFile(int $id) 
    {
        return $this->files()->findOrFail($id);
    }      
    
    /**
     * Query to get todos by user id
     * 
     * @param int $userId
     * 
     * @return Todo
     */     
    private static function list(int $userId)
    {
        return self::forUser($userId)->with(['user', 'colors', 'importance', 'group']);
    }

    /**
     * Query to get todos for reminder
     * 
     * @return array[Todo]
     */     
    public static function reminders()
    {
        $todos = self::with(['user'])
                    ->where('is_reminded', false)
                    ->whereDate('reminder_at', Carbon::now())
                    ->whereRaw('DATE_FORMAT(reminder_at, "%H:%i") = "'.Carbon::now()->format('H:i').'"')
                    ->get();

        foreach ($todos as $todo)
        {
            $todo->update([
                'is_reminded' => true
            ]);
        }

        return $todos;
    }    

    /* Relations */

    /*
     * The colors that belong to the todo.
     */
    public function colors()
    {
        return $this->belongsToMany('App\Models\Todo\Color', 'colors_todos', 'color_id', 'todo_id');
    }    

    /*
     * The importance related to the todo
     */    
    public function importance() 
    {
        return $this->belongsTo('App\Models\Todo\Importance', 'importance_id', 'id');
    }    

    /*
     * The group related to the todo
     */    
    public function group() 
    {
        return $this->belongsTo('App\Models\Todo\Group', 'group_id', 'id');
    }   
    
    /*
     * The files related to the todo
     */    
    public function files() 
    {
        return $this->hasMany('App\Models\Todo\File', 'todo_id', 'id');
    }     

    /*
     * The user related to the todo
     */    
    public function user() 
    {
        return $this->belongsTo('App\Models\User\User', 'user_id', 'id');
    }     
    
    /* Scopes */

    /**
     * Todo with specified user id
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $userId
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public function scopeForUser($query, int $userId)
    {
        return $query->where('user_id', $userId);
    }      
}
