<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    // Related table
    protected $table = 'tokens';

    // Timestamp columns usage
    public $timestamps = true;

    // Fillable fields
    protected $fillable = [
        'user_id',
        'token',
        'ip',
        'device',
    ];

    /* Methods */

    /**
     * Generate new token and assign it to user
     * 
     * @param int $userId
     * 
     * @return string
     */    
    public static function generate(int $userId) 
    {
        $model = [
            'user_id' => $userId,
            'token' => hash('sha1', md5(microtime())),
            'ip' => \Request::ip(),
            'device' => \Browser::userAgent()
        ];

        return self::create($model)->token;
    }

    /**
     * Remove token by token
     * 
     * @param string $token
     * 
     * @return bool
     */    
    public static function deleteByToken(string $token) 
    {
        return (bool) self::where('token', $token)->delete();
    }    

    /**
     * Remove all tokens by user id
     * 
     * @param int $userId
     * 
     * @return bool
     */    
    public static function deleteByUser(int $userId) 
    {
        return (bool) self::where('user_id', $userId)->delete();
    }  
    
    /**
     * Get all tokens by user id
     * 
     * @param int $userId
     * 
     * @return array
     */    
    public static function devices(int $userId) 
    {
        return self::where('user_id', $userId)->select(['id', 'device', 'ip', 'created_at', 'updated_at'])->orderBy('updated_at', 'desc')->get();
    }    
    
    /**
     * Remove token by user id and device (token) id
     * 
     * @param int $userId
     * @param int $deviceId
     * 
     * @return bool
     */    
    public static function deleteByDevice(int $userId, int $deviceId) 
    {
        return (bool) self::where([
            ['user_id', '=', $userId],
            ['id', '=', $deviceId]
        ])->delete();
    }     
}
