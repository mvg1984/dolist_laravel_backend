<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

// Facades
use DB;
use Hash;

// Models
use App\Models\User\Token;

class User extends Model
{
    // Related table
    protected $table = 'users';

    // Timestamp columns usage
    public $timestamps = true;

    // Fillable fields
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_verified'
    ];

    // Hidden columns
    protected $hidden = [
        'password', 
        'is_verified',
    ];  
    

    // Append columns
    protected $appends = [
        'channel', 
    ];     

    // Password encoding algorythm
    private $encoding = 'sha256';

    /* Mutators */

    /**
     * Hash password before store or update
     *
     * @param string  $value
     * 
     * @return void
     */    
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = hash($this->encoding, $value);
    }  
    
    /**
     * Get channel name for pusher
     *
     * @param string  $value
     * 
     * @return string
     */    
    public function getChannelAttribute()
    {
        return md5($this->attributes['name'].$this->attributes['password']);
    }    
    

    /* Methods */

    /**
     * Create user
     * 
     * @param array $input
     * 
     * @return User
     */    
    public static function registration(array $input) 
    {
        return self::create($input);
    }

    /**
     * Activate user
     * 
     * @param string $hash
     * 
     * @return bool
     */    
    public static function activation(string $hash) 
    {
        return (bool) self::hash($hash)->verified(false)->update([
            'is_verified' => true
        ]);
    }  
    
    /**
     * Authorize user (generate token)
     * 
     * @param array $input
     * 
     * @return string
     */    
    public static function token(array $input) 
    {
        if (!($user = self::credentials($input['email'], $input['password'])->verified(true)->first()))
        {
            return null;
        }

        return Token::generate($user->id);
    }         

    /* Scopes */

    /**
     * User with hash
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $hash
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */      
    public function scopeHash($query, $hash)
    {
        return $query->where(DB::raw('MD5(email)'), $hash);
    }    

    /**
     * User with verified mark
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $mark
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public function scopeVerified($query, string $mark)
    {
        return $query->where('is_verified', $mark);
    }    
    
    /**
     * User with email and password
     * 
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $email
     * @param string $password
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */    
    public function scopeCredentials($query, string $email, string $password)
    {
        return $query->where('email', $email)->where('password', hash($this->encoding, $password));
    }      
}
