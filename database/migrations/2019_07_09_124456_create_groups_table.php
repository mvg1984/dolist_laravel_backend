<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191)->index();
            $table->string('code', 191)->index();
        });

        \DB::table('groups')->insert([
            'name' => 'Home',
            'code' => 'home',
        ]); 
        
        \DB::table('groups')->insert([
            'name' => 'Work',
            'code' => 'work',
        ]);  
        
        \DB::table('groups')->insert([
            'name' => 'Shopping',
            'code' => 'shopping',
        ]);  
        
        \DB::table('groups')->insert([
            'name' => 'Other',
            'code' => 'other',
        ]);         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
