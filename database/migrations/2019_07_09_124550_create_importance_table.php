<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191)->index();
            $table->string('code', 191)->index();
        });

        \DB::table('importances')->insert([
            'name' => 'Low',
            'code' => 'low',
        ]);           

        \DB::table('importances')->insert([
            'name' => 'Normal',
            'code' => 'normal',
        ]);        

        \DB::table('importances')->insert([
            'name' => 'Hot',
            'code' => 'hot',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('importance');
    }
}
