<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191)->index();
            $table->string('code', 191)->index();
        });

        \DB::table('colors')->insert([
            'name' => 'Red',
            'code' => '#a40b0b',
        ]); 
        
        \DB::table('colors')->insert([
            'name' => 'Green',
            'code' => '#1abc9c',
        ]);  
        
        \DB::table('colors')->insert([
            'name' => 'Blue',
            'code' => '#03396c',
        ]);  
        
        \DB::table('colors')->insert([
            'name' => 'Orange',
            'code' => '#f36b2c',
        ]);        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
    }
}
