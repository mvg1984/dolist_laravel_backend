## Description

Some time ago during one interview, i got a test task to develop simple Todo API to demonstrate some knowleges of the Laravel, principles of RESTful, and code-writing example. 

So, i decided to develop several really simple applications for the future for the reasons i wrote above, using the tools and frameworks where i have experience:
- <a href='https://gitlab.com/mvg1984/dolist_laravel_backend'>PHP Laravel Todo API with MySQL + PHP Laravel Web application (with Vuejs) for the Todo API</a>
- <a href='https://gitlab.com/mvg1984/dolist_vuejs_client'>Vue SPA client for the Todo API</a>
- <a href='https://gitlab.com/mvg1984/dolist_react_client'>React SPA client for the Todo API</a>
- React Native App client for the Todo API
- Angularjs SPA for the Todo API (Angularjs it is not Angular 6-7-8-9... :))
- Nodejs Nest Todo API with MongoDB
- Golang Todo API with MySQL

Of course, it is really simple tasks :), but it can demonstrate a little bit how i write the code, how i structure applications, how i use patterns, how i use specific tools, libraries and other capabilities of each framework, how i test it and so on. And... at least it shows that i know what it is and how to use it :D

## Installation

Copy '.env.example' to the '.env' and change MySQL and SMTP settings

```
composer install
php artisan key:generate 
php artisan migrate
```

## Todo notifications

Add command to the crontab...

```
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1=
```

...and run job listener

```
php artisan queue:work
```

## Browser notifications

Frontend supports browser push notifications via pusher.com. You can set pusher credentials in the .env file.

```
PUSHER_APP_ID=1111111
PUSHER_APP_KEY=1c17a210d1b2816efae7
PUSHER_APP_SECRET=61151e118a16811761f4
PUSHER_APP_CLUSTER=eu
```