<h2>Notification</h2>

<p>Hello, {{ $todo->user->name }}! You asked Dolist to remind you about the todo.</p>

<p><b>{{ $todo->name }}</b></p>

@if($todo->deadline_at)
    <p> Deadline at {{ Carbon\Carbon::parse($todo->deadline_at)->format('d.m.Y - H:i') }}</p>
@endif