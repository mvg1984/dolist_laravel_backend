<h2>Hello, {{ $userName }}</h2>

<p>Activate your account by this link: <a href='{{ $activationLink }}'>{{ $activationLink }}</a></p>