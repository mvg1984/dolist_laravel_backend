@extends('layouts.master')
@section('title', 'Error')

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center" style="height:100vh">
        <div class="alert alert-danger" role="alert">{{ $error }}</div>
    </div>
</div>
@endsection