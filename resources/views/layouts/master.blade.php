<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link href="//fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/styles.css?{{ time() }}">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="//code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        @if(!env('APP_DEBUG'))
        <script src="//unpkg.com/vue/dist/vue.min.js"></script> <!-- Production -->
        @else
        <script src="//cdn.jsdelivr.net/npm/vue"></script> <!-- Development -->
        @endif
        <script src="//js.pusher.com/5.0/pusher.min.js"></script>
        <script src="//unpkg.com/lodash"></script>
        <script src="//unpkg.com/moment"></script>
        <script src="//unpkg.com/numeral"></script>
        <script src="//unpkg.com/axios/dist/axios.min.js"></script>
        <script src="//unpkg.com/vuelidate/dist/vuelidate.min.js"></script>
        <script src="//unpkg.com/vuelidate/dist/validators.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>

        <title>@yield('title') - {{ env('APP_NAME') }}</title>
    </head>
    <body>
        @yield('content')

        @if(isset($userName))
        <script>
            // Permissions for notifications
            Notification.requestPermission();

            // Pusher for reminders
            Pusher.logToConsole = true;

            var pusher = new Pusher('{{ env("PUSHER_APP_KEY") }}', {
                cluster: '{{ env("PUSHER_APP_CLUSTER") }}'
            });

            pusher.subscribe('{{ $channel }}').bind('reminder-sent', function(data) {
                var message = '{{ env("APP_NAME") }} reminds yout about "' + data.todo.name + '"';
                var url  = '{{ env("APP_URL") }}/' + data.todo.id;

                if (Notification.permission === "granted") {
                    var notification = new Notification('Dolist reminder', {
                        body: message
                    });

                    notification.onclick = function() {
                        location.href = url;
                    };                    
                }
                else {
                    if(confirm(message)) {
                        location.href = url;
                    }
                }
            });
        </script>
        @endif
    </body>
</html>