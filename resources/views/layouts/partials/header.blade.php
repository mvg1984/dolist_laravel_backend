<div class="row mt-3">
    <div class="col-6">
        Hello, <a href="{{ route('profile.index') }}">{{ $userName }}</a>. This is Dolist application.
    </div>
    <div class="col-6">
        <a href="{{ route('users.logout.action') }}" class="btn btn-light float-right" role="button" aria-pressed="true">Log out</a>
    </div>
</div>
<hr/>