@extends('layouts.master')
@section('title', 'Profile')

@section('content')
<div class="container">
    @include('layouts.partials.header')
    <div class="row mt-3">
        <div class="col-6">
            <a href="{{ route('todos.index') }}" class="btn btn-link" role="button" aria-pressed="true">Back to the list</a>
        </div>        
    </div>
    <div class="row mt-3">
        <div class="col-12">
            @include('layouts.partials.errors')
        </div>
        <div class="col-6">    
            <div class="card">
                <div class="card-header">
                    Change name
                </div>
                <div class="card-body">
                    <form action="{{ route('profile.name.action') }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="name" value="{{ old('name', $userName) }}" required placeholder="Your new name" class="form-control" name="name">
                        </div>
                        <button type="submit" class="btn btn-primary">Change</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">    
            <div class="card">
                <div class="card-header">
                    Change password
                </div>
                <div class="card-body">
                    <form action="{{ route('profile.password.action') }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="password" value="{{ old('password', '') }}" required placeholder="Your new password" class="form-control" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Change</button>
                    </form>
                </div>
            </div>
        </div>        
    </div>
    <div class="row mt-3">
        <div class="col-12">    
            <div class="card">
                <div class="card-header">
                    Authorized tokens and devices
                </div>
                <div class="card-body card-table">
                    <table class="table devices">
                        <thead>
                            <tr>
                                <td>Device</td>
                                <td width="120">IP address</td>
                                <td width="150">Created</td>
                                <td width="150">Last used</td>
                                <td width="100">&nbsp;</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($devices as $device)
                                <tr>
                                    <td>{{ $device['device'] }}</td>
                                    <td>{{ $device['ip'] }}</td>
                                    <td>{{ Carbon\Carbon::parse($device['created_at'])->format('d.m.Y, H:i') }}</td>
                                    <td>{{ Carbon\Carbon::parse($device['updated_at'])->format('d.m.Y, H:i') }}</td>
                                    <td><a href="{{ route('profile.delete_device.action', ['id' => $device['id']]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm float-right" role="button" aria-pressed="true">Log out</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection