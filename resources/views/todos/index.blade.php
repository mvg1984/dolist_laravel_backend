@extends('layouts.master')
@section('title', 'Todo list')

@section('content')
<div class="container">
    @include('layouts.partials.header')
    <div class="row mt-3">
        <div class="col-12">
            <a href="{{ route('todos.new') }}" class="btn btn-primary float-right" role="button" aria-pressed="true">Create new todo</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">    
            <div class="card">
                <div class="card-header">
                    @yield('title')
                </div>
                <div class="card-body card-table">
                    <table class="table">
                        <tbody>
                            @foreach($list as $item)
                            <tr>
                                <td><a href="{{ route('todos.show', ['id' => $item['id']]) }}">{{ $item['name'] }}</a></td>
                                <td width="150">{{ Carbon\Carbon::parse($item['created_at'])->format('d.m.Y, H:i') }}</td>
                                <td width="70" class="d-none d-lg-table-cell">{{ $item['group']['name'] }}</td>
                                <td width="90" class="d-none d-lg-table-cell">
                                    @foreach($item['colors'] as $color)
                                        <span class="badge" style="background-color: {{ $color['code'] }}">&nbsp;</span>
                                    @endforeach
                                </td>
                                <td width="100"><a href="{{ route('todos.remove', ['id' => $item['id']]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm float-right" role="button" aria-pressed="true">Remove</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection