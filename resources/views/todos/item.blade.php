@extends('layouts.master')
@section('title', isset($item) ? $item['name'] : 'New todo')

@section('content')
<div class="container" id="todo" v-cloak>
    @include('layouts.partials.header')
    <form ref="form" method="post" enctype="multipart/form-data" action="{{ isset($item['id']) ? route('todos.update', ['id' => $item['id']]) : route('todos.create') }}">
        {{ csrf_field() }}
        <div class="row mt-3">
            <div class="col-6">
                <a href="{{ route('todos.index') }}" class="btn btn-link" role="button" aria-pressed="true">Back to the list</a>
            </div>        
            <div class="col-6">
                <a href="" v-on:click.prevent="save()" class="btn btn-primary float-right" role="button" aria-pressed="true">Save</a>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">    
                <div class="card">
                    <div class="card-header">
                        @yield('title')
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="todo-name">Todo name</label>
                                    <input v-model="model.name" :class="{ 'is-invalid': $v.model.name.$error }" name="name" type="text" class="form-control" id="todo-name" aria-describedby="todo-name-help" placeholder="Todo name">
                                    <small id="todo-name-help" class="form-text text-muted">Write the short name of your todo</small>
                                </div>
                                <div class="form-group">
                                    <label for="todo-group">Group</label>
                                    <select v-model="model.group_id" class="form-control" name="group_id" id="todo-group" aria-describedby="todo-group-help">
                                        <option v-for="item in dictionaries.groups" :value="item.id">@{{ item.name }}</option>
                                    </select>
                                    <small id="todo-group-help" class="form-text text-muted">Choose the group of your todo</small>
                                </div> 
                                <div class="form-group">
                                    <label for="todo-importance">Importance</label>
                                    <select v-model="model.importance_id" class="form-control" name="importance_id" id="todo-importance" aria-describedby="todo-importance-help">
                                        <option v-for="item in dictionaries.importances" :value="item.id">@{{ item.name }}</option>
                                    </select>
                                    <small id="todo-importance-help" class="form-text text-muted">Choose the importance of your todo</small>
                                </div>                                                                
                                <div class="form-group">
                                    <label for="todo-description">Description</label>
                                    <textarea v-model="model.description" name="description" class="form-control" id="todo-description" rows="6" aria-describedby="todo-description-help"></textarea>
                                    <small id="todo-description-help" class="form-text text-muted">Write some description about your todo</small>
                                </div>                                   
                            </div>
                            <div class="col-12 col-md-6"> 
                                <div class="form-group">
                                    <label for="todo-description"><a href="" v-on:click.prevent="useDeadline = !useDeadline">@{{ useDeadline ? 'Unset' : 'Set'}} deadline</a></label>
                                    <div class="inline-fields" v-if="useDeadline">
                                        <input v-model="model.deadline_date" v-mask="'##.##.####'" :class="{ 'is-invalid': $v.model.deadline_date.$error }" name="deadline_date" type="text" style="width: 125px;" class="form-control" id="todo-deadline_date" aria-describedby="todo-deadline_at-help" placeholder="dd.mm.yyyy">                            
                                        <input v-model="model.deadline_time" v-mask="'##:##'" :class="{ 'is-invalid': $v.model.deadline_time.$error }" name="deadline_time" type="text" style="width: 85px;" class="form-control" id="todo-deadline_time" aria-describedby="todo-deadline_at-help" placeholder="hh:hh">                            
                                    </div>
                                    <small v-if="useDeadline" id="todo-deadline_at-help" class="form-text text-muted">Set deadline to your todo</small>
                                </div>    
                                <div class="form-group">
                                    <label for="todo-description"><a href="" v-on:click.prevent="useReminder = !useReminder">@{{ useReminder ? 'Unset' : 'Set'}} reminder</a></label>
                                    <div class="inline-fields" v-if="useReminder">
                                        <input v-model="model.reminder_date" v-mask="'##.##.####'" :class="{ 'is-invalid': $v.model.reminder_date.$error }" name="reminder_date" type="text" style="width: 125px;" class="form-control" id="todo-reminder_date" aria-describedby="todo-reminder_at-help" placeholder="dd.mm.yyyy">                            
                                        <input v-model="model.reminder_time" v-mask="'##:##'" :class="{ 'is-invalid': $v.model.reminder_time.$error }" name="reminder_time" type="text" style="width: 85px;" class="form-control" id="todo-reminder_time" aria-describedby="todo-reminder_at-help" placeholder="hh:hh">                            
                                    </div>
                                    <small v-if="useReminder" id="todo-reminder_at-help" class="form-text text-muted">Set reminder to your todo</small>
                                </div>                                                                                       
                                <div class="form-group">
                                    <label for="todo-importance">Colors</label>
                                    <div v-for="item in dictionaries.colors" class="form-check">
                                        <input class="form-check-input" v-model="model.colors" type="checkbox" name="colors_id[]" :value="item.id" :id="'color' + item.id">
                                        <label class="form-check-label" :style="'color:' + item.code" :for="'color' + item.id">@{{ item.name }}</label>
                                    </div>                                     
                                </div>   
                                <div class="form-group">
                                    <label for="todo-importance">Files</label>
                                    <div class="custom-file">
                                        <input type="file" v-on:change="selectFile" class="custom-file-input" name="file" id="file">
                                        <label class="custom-file-label" for="file">@{{ file }}</label>
                                    </div>
                                    @if(isset($files))                                                                       
                                        <table class="file-list">
                                            @foreach($files as $file)   
                                                <tr>
                                                    <td><a href="{{ route('todos.files.download', ['id' => $item['id'], 'fileId' => $file['id']]) }}">{{ $file['label'] }}</a></td>
                                                    <td width="120">{{ number_format($file['size'], 0) }} bytes</td>
                                                    <td width="80"><a href="{{ route('todos.files.remove', ['id' => $item['id'], 'fileId' => $file['id']]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm float-right" role="button" aria-pressed="true">Remove</a></td>
                                                </tr>  
                                            @endforeach                                          
                                        </table>
                                    @endif
                                </div>                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>

// Connect mask library
Vue.use(VueMask.VueMaskPlugin);

// Connect validation library
Vue.use(window.vuelidate.default);
let { required, requiredIf, minLength } = window.validators;

// Custome validator for dd.mm.yyyy format
let dmy = function(value) 
{ 
    if (!value || value == '') 
    {
        return true;
    }
    return /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/.test(value); 
};

// Custome validator for hh:mm format
let hs = function(value) 
{ 
    if (!value || value == '') 
    {
        return true;
    }
    return /^([0-9]{2})\:([0-9]{2})$/.test(value); 
};

// Todo model
@if(isset($item))
    let model = @json($item);

    // Extend model
    if (model.deadline_at && model.deadline_at != '')
    {
        model.deadline_date = moment(model.deadline_at).format('DD.MM.YYYY');
        model.deadline_time = moment(model.deadline_at).format('HH:mm');        
    }    

    if (model.reminder_at && model.reminder_at != '')
    {
        model.reminder_date = moment(model.reminder_at).format('DD.MM.YYYY');
        model.reminder_time = moment(model.reminder_at).format('HH:mm');        
    }   

    console.log(model);
@else 
    let model = {
        name: null,
        group_id: '1',
        importance_id: '1',
        description: null,
        deadline_at: null,
        reminder_at: null,
        colors: []
    };
@endif

// Dictionaries
let dictionaries = {
    groups: @json($groups),
    importances: @json($importances),
    colors: @json($colors)
};

// Vue application
let todo = new Vue({
    el: '#todo',
    data: {
        useDeadline: Boolean(model.deadline_at),
        useReminder: Boolean(model.reminder_at),
        model: model,
        file: 'Choose file',
        dictionaries: dictionaries
    },
    validations: {
        model: {
            name: { required },
            deadline_date: { required: requiredIf(function() { return this.useDeadline; }), dmy },
            deadline_time: { required: requiredIf(function() { return this.useDeadline; }), hs },
            reminder_date: { required: requiredIf(function() { return this.useReminder; }), dmy },
            reminder_time: { required: requiredIf(function() { return this.useReminder; }), hs }            
        }
    },
    computed: {
        isWidthDeadline() 
        {
            console.log(123);
            return false;
        }
    },    
    mounted: function() 
    {

    },    
    filters: {

    },
    methods: {
        save: function() 
        {
            // Run validation
            this.$v.$touch();
            if (this.$v.$invalid) 
            {
                return false;
            }

            // Submit form
            this.$refs.form.submit();
        },
        selectFile()
        {
            let files = document.getElementById('file').files;
            if(files.length == 0) 
            {
                this.file = 'Choose file';
            }
            else
            {
                this.file = files[0].name;
            }
        }
    }
});

// Directives

</script>

@endsection