@extends('layouts.master')
@section('title', 'Registration')

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center" style="height:100vh">
        <div class="col-12 col-lg-4 col-md-6 col-sm-8">
        @include('layouts.partials.errors')
            <div class="card">
                <div class="card-header">
                Registration
                </div>
                <div class="card-body">
                    <form action="{{ route('users.registration.action') }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" value="{{ old('name', '') }}" required placeholder="Name" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <input type="email" value="{{ old('email', '') }}" required placeholder="E-mail" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{ old('password', '') }}" required autocomplete="none" placeholder="Password" class="form-control" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Create account</button>
                    </form>
                </div>
            </div>
            <div class="row justify-content-center align-items-center mt-2">
                <a href="{{ route('users.authorization.view') }}">Authorization</a>
            </div>
        </div>
    </div>
</div>
@endsection