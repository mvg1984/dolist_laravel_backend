<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Registration and authorization
Route::prefix('/users')->group(function () 
{
    Route::post('/registration', ['uses' => 'Api\UserController@registration'])->name('users.registration');
    Route::get('/activation/{hash}', ['uses' => 'Api\UserController@activation'])->name('users.activation');
    Route::post('/authorization', ['uses' => 'Api\UserController@authorization'])->name('users.authorization');
});

Route::middleware('token')->group(function () 
{
    // Profile
    Route::prefix('/profile')->group(function () 
    {
        Route::get('/', ['uses' => 'Api\ProfileController@index'])->name('profile.index');
        Route::delete('/logout/', ['uses' => 'Api\ProfileController@logout'])->name('profile.logout');
        Route::delete('/logout/all', ['uses' => 'Api\ProfileController@logoutAll'])->name('profile.logoutAll');
        Route::get('/devices/', ['uses' => 'Api\ProfileController@devices'])->name('profile.devices');
        Route::delete('/devices/{id}', ['uses' => 'Api\ProfileController@logoutDevice'])->name('profile.logoutDevice');
        Route::put('/change/name', ['uses' => 'Api\ProfileController@changeName'])->name('profile.changeName');
        Route::put('/change/password', ['uses' => 'Api\ProfileController@changePassword'])->name('profile.changePassword');
    }); 
    
    // Todos
    Route::prefix('/todos')->group(function () 
    {
        Route::get('/', ['uses' => 'Api\TodoController@index'])->name('todos.index');
        Route::post('/', ['uses' => 'Api\TodoController@create'])->name('todos.create');
        Route::get('/{id}', ['uses' => 'Api\TodoController@show'])->where('id', '[0-9]+')->name('todos.show');
        Route::put('/{id}', ['uses' => 'Api\TodoController@update'])->where('id', '[0-9]+')->name('todos.update');
        Route::delete('/{id}', ['uses' => 'Api\TodoController@destroy'])->where('id', '[0-9]+')->name('todos.destroy');

        Route::post('/{id}/file', ['uses' => 'Api\TodoController@attach'])->where('id', '[0-9]+')->name('todos.file.attach');
        Route::get('/{id}/file', ['uses' => 'Api\TodoController@files'])->where('id', '[0-9]+')->name('todos.file.list');
        Route::get('/{id}/file/{fileId}', ['uses' => 'Api\TodoController@download'])->where('id', '[0-9]+')->where('fileId', '[0-9]+')->name('todos.file.download');
        Route::delete('/{id}/file/{fileId}', ['uses' => 'Api\TodoController@erase'])->where('id', '[0-9]+')->where('fileId', '[0-9]+')->name('todos.file.erase');

        // Dictionaries for todo
        Route::prefix('/dictionaries')->group(function () 
        {
            Route::get('/colors', ['uses' => 'Api\TodoController@colors'])->name('todos.colors');
            Route::get('/groups', ['uses' => 'Api\TodoController@groups'])->name('todos.groups');
            Route::get('/importances', ['uses' => 'Api\TodoController@importances'])->name('todos.importances');
        });
    });  
    
    // System (NOT FOR PRODUCTION)
    Route::prefix('/system')->group(function () 
    {
        Route::get('/todos-for-reminder', ['uses' => 'Api\TodoController@notifications'])->name('todo.notifications');
    });     
});