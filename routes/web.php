<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Anonymous
Route::middleware(['anonymous'])->group(function () 
{
    // Authorization
    Route::get('/authorization', ['uses' => 'Web\UserController@authorization'])->name('users.authorization.view');
    Route::post('/authorization', ['uses' => 'Web\UserController@_authorization'])->name('users.authorization.action');

    // Registration
    Route::get('/registration', ['uses' => 'Web\UserController@registration'])->name('users.registration.view');
    Route::post('/registration', ['uses' => 'Web\UserController@_registration'])->name('users.registration.action');

    // Activation
    Route::get('/activation/{hash}', ['uses' => 'Web\UserController@activation'])->name('users.activation.action');
});

// Authorized
Route::middleware(['authorization'])->group(function () 
{
    // Logout
    Route::get('/logout', ['uses' => 'Web\UserController@_logout'])->name('users.logout.action');

    // Todos
    Route::get('/', ['uses' => 'Web\TodoController@index'])->name('todos.index');
    Route::get('/new', ['uses' => 'Web\TodoController@new'])->name('todos.new');
    Route::post('/', ['uses' => 'Web\TodoController@create'])->name('todos.create');
    Route::get('/{id}', ['uses' => 'Web\TodoController@show'])->where('id', '[0-9]+')->name('todos.show');
    Route::post('/{id}', ['uses' => 'Web\TodoController@update'])->where('id', '[0-9]+')->name('todos.update');
    Route::get('/{id}/delete', ['uses' => 'Web\TodoController@remove'])->where('id', '[0-9]+')->name('todos.remove');
    Route::get('/{id}/download/{fileId}', ['uses' => 'Web\TodoController@downloadFile'])->where('id', '[0-9]+')->where('fileId', '[0-9]+')->name('todos.files.download');
    Route::get('/{id}/remove/{fileId}', ['uses' => 'Web\TodoController@removeFile'])->where('id', '[0-9]+')->where('fileId', '[0-9]+')->name('todos.files.remove');

    // Profile
    Route::prefix('/profile')->group(function () 
    {
        Route::get('/', ['uses' => 'Web\ProfileController@index'])->name('profile.index');
        Route::post('/name', ['uses' => 'Web\ProfileController@changeName'])->name('profile.name.action');
        Route::post('/password', ['uses' => 'Web\ProfileController@changePassword'])->name('profile.password.action');
        Route::get('/devices/{id}', ['uses' => 'Web\ProfileController@deleteDevice'])->name('profile.delete_device.action');
    });
});