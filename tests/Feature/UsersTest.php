<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use \Tests\Helper;

    /**
     * API Authorization test
     *
     * @return void
     */
    public function testAuthorization()
    {
        // Request
        $request = $this->_post(route('users.authorization'), [
            'email' => 'test@test.com',
            'password' => '12345',
        ]);

        // Response data
        $response = json_decode($request->getContent(), true);

        // Asserts
        $request
            ->assertStatus(200)
            ->assertJson([
                'result' => true
            ]);

        // Store token for all tests in this class
        $token = &$this->token();
        $token = $response['data'];
    }    

    /**
     * API Logout test
     *
     * @return void
     */
    public function testLogout()
    {
        $token = &$this->token();

        // Request
        $request = $this->_delete(route('profile.logout'), [], [
            'Dolist-Token' => $token
        ]);

        // Response data
        $response = json_decode($request->getContent(), true);

        // Asserts
        $request
            ->assertStatus(200)
            ->assertJson([
                'result' => true
            ]);
    } 
}
