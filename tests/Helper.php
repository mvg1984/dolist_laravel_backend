<?php

namespace Tests;

trait Helper 
{
    protected function &token()
    {
        static $token = null;
        return $token;
    }

    // POST
    public function _post($url, $body = [], $headers = [])
    {
        return $this->withHeaders($headers)->json('POST', $url, $body);
    }

    // GET
    public function _get($url, $body = [], $headers = [])
    {
        return $this->withHeaders($headers)->json('GET', $url, $body);
    }   
    
    // DELETE
    public function _delete($url, $body = [], $headers = [])
    {
        return $this->withHeaders($headers)->json('DELETE', $url, $body);
    }  
}