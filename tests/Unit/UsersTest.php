<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

// Models
use App\Models\User\User;

// Facades
use Log;

class UsersTest extends TestCase
{
    /**
     * Пример теста.
     *
     * @return void
     */
    public function testCheckTestUser()
    {
        $this->assertEquals(1, User::where('email', 'test@test.com')->count());
    }         
}
